package hadoop.rpc.studentid;

import hadoop.rpc.studentid.rpcinterface.MyInterface;
import hadoop.rpc.studentid.rpcinterface.StudentIDInterface;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * @author even
 * @Description:
 * @date 2021/7/25
 */
public class Client {

    public static void main(String[] args) {
        try {
            StudentIDInterface proxy = RPC.getProxy(StudentIDInterface.class,1L,new InetSocketAddress(12300),new Configuration());
            String studentName = proxy.getRealName("123");
            System.out.println(studentName);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
