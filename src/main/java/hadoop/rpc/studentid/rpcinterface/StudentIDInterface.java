package hadoop.rpc.studentid.rpcinterface;

import org.apache.hadoop.ipc.VersionedProtocol;

/**
 * @author even
 * @Description:
 * @date 2021/7/25
 */
public interface StudentIDInterface extends VersionedProtocol {

    long versionID = 1L;
    String getRealName(String studentID);
}
