package hadoop.rpc.studentid.server;

import hadoop.rpc.studentid.rpcinterface.StudentIDInterface;
import org.apache.hadoop.ipc.ProtocolSignature;

import java.io.IOException;

/**
 * @author even
 * @Description:
 * @date 2021/7/25
 */
public class StudentIDImpl implements StudentIDInterface {

    @Override
    public String getRealName(String studentID) {
        if("G20210735010442".equals(studentID)){
            return "黄奕文";
        }
        return null;
    }

    @Override
    public long getProtocolVersion(String protocol, long clientVersion) throws IOException {
        return 0;
    }

    @Override
    public ProtocolSignature getProtocolSignature(String protocol, long clientVersion, int clientMethodsHash) throws IOException {
        return null;
    }
}
