package hadoop.rpc.studentid.server;

import hadoop.rpc.studentid.rpcinterface.MyInterface;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.ProtocolSignature;
import org.apache.hadoop.ipc.RPC;

import java.io.IOException;

/**
 * @author even
 * @Description:
 * @date 2021/7/25
 */
public class MyInterfaceImpl implements MyInterface {

    @Override
    public int add(int number1, int number2) {
        System.out.println("number1 = " + number1 + "number2 = " + number2 );
        return number1+number2;
    }

    @Override
    public long getProtocolVersion(String protocol, long clientVersion) throws IOException {
        return MyInterface.versionID;
    }

    @Override
    public ProtocolSignature getProtocolSignature(String protocol, long clientVersion, int clientMethodsHash) throws IOException {
        return null;
    }
}
