package hadoop.rpc.studentid;

import hadoop.rpc.studentid.rpcinterface.MyInterface;
import hadoop.rpc.studentid.rpcinterface.StudentIDInterface;
import hadoop.rpc.studentid.server.MyInterfaceImpl;
import hadoop.rpc.studentid.server.StudentIDImpl;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;

import java.io.IOException;

/**
 * @author even
 * @Description:
 * @date 2021/7/25
 */
public class Server {

    public static void main(String[] args) {
        RPC.Builder builder = new RPC.Builder(new Configuration());
        builder.setBindAddress("127.0.0.1");
        builder.setPort(12300);

        builder.setProtocol(StudentIDInterface.class);
        builder.setInstance(new StudentIDImpl());

        try {
            RPC.Server server = builder.build();
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
