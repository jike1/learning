作业：Hadoop RPC
根据文档中的示例，完成一个类似的 RPC 函数，要求：

输入你的真实学号，返回你的真实姓名
输入学号 20210000000000，返回 null
输入学号 20210123456789，返回心心

代码:
Server : hadoop.rpc.studentid.Server
Client : hadoop.rpc.studentid.Client

RPC接口 : hadoop.rpc.studentid.rpcinterface.StudentIDInterface
代理类 : hadoop.rpc.studentid.server.StudentIDImpl

运行结果:
Server端运行结果:
![img.png](ServerResult.png)

Client 运行结果:
输入我的学号:
![img.png](Client_1.png)
输入123:
![img.png](Client_2.png)



