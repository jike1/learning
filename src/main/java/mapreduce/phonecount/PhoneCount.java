package mapreduce.phonecount;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import util.HDFSUtil;

/**
 * @author even
 * @Description:
 * @date 2021/7/17
 */
public class PhoneCount extends Configured implements Tool {

    @Override
    public int run(String[] args) throws Exception {
        Configuration configuration = super.getConf();

        String inputPath = args[0];
        String outputPath = args[1];

        Job job = Job.getInstance(configuration,"HuangYiWen-phoneCounter");

        job.setJarByClass(PhoneCount.class);

        job.setMapperClass(PhoneCountMapper.class);
        job.setReducerClass(PhoneCountReducer.class);
        job.setCombinerClass(PhoneCountReducer.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(FlowBean.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(FlowBean.class);

        job.setNumReduceTasks(1);

        FileInputFormat.addInputPath(job,new Path(inputPath));

        HDFSUtil.checkPath(outputPath,configuration,true);
        System.out.println("outputPath: " + outputPath);
        FileOutputFormat.setOutputPath(job, new Path(outputPath));

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new PhoneCount(), args));
    }

}
