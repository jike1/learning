
# 极客时间 手机用户流量需求统计 MapReduce实现

1. 需求：
统计每一个手机号耗费的总上行流量、下行流量、总流量

2. 数据准备：
(1)输入数据格式：
时间戳、电话号码、基站的物理地址、访问网址的ip、网站域名、数据包、接包数、上行/传流量、下行/载流量、响应码
![img.png](img.png)
(2)最终输出的数据格式：
手机号码		上行流量        下行流量		总流量
![img_1.png](img_1.png)

3. 代码说明:
   主类：mapreduce.phonecount.PhoneCount
   输入参数：
      参数1 : HDFS输入路径
      参数2 : HDFS输出路径
4. 结果：
    i. 代码路径:极客阿里云服务器/home/student/even/mapreduce/phone_count
   ii. 输入路径:HDFS: /user/student/even/mapreduce/phone_count/input/HTTP_20130313143750.dat
   iii. 输出路径:HDFS: /user/student/even/mapreduce/phone_count/output
   iv. yarn UI链接 : http://jikehadoop03:19888/jobhistory/job/job_1626505659980_0029
   ![img_2.png](img_2.png)