package mapreduce.phonecount;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author even
 * @Description:
 * @date 2021/7/17
 */
public class PhoneCountReducer extends Reducer<Text, FlowBean,Text,FlowBean> {

    private FlowBean outputValue = new FlowBean();

    @Override
    protected void reduce(Text key, Iterable<FlowBean> values, Context context) throws IOException, InterruptedException {
        long upFlow = 0;
        long downFlow = 0;
        long sumFlow = 0;
        for (FlowBean value : values) {
            upFlow += value.getUpFlow();
            downFlow += value.getDownFlow();
            sumFlow += value.getSumFlow();
        }
        outputValue.setUpFlow(upFlow);
        outputValue.setDownFlow(downFlow);
        outputValue.setSumFlow(sumFlow);
        context.write(key,outputValue);
    }
}
