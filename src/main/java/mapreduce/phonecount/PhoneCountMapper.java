package mapreduce.phonecount;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.junit.Test;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 * @author even
 * @Description:
 * @date 2021/7/17
 */
public class PhoneCountMapper extends Mapper<LongWritable, Text, Text, FlowBean> {

    private String line;
    private String[] segments;
    private Text outputKey = new Text();
    private FlowBean outputValue = new FlowBean();
    private long upFlow;
    private long downFlow;

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        line = value.toString();
        segments = line.split("\\s+");
        outputKey.set(segments[1]);
        upFlow = Long.parseLong(segments[segments.length-3]);
        downFlow = Long.parseLong(segments[segments.length-2]);
        outputValue.setUpFlow(upFlow);
        outputValue.setDownFlow(downFlow);
        outputValue.setSumFlow(upFlow + downFlow);
        context.write(outputKey,outputValue);
    }

    @Test
    public void testSplit(){
        String line = "1363157986041 \t13480253104\t5C-0E-8B-C7-FC-80:CMCC-EASY\t120.197.40.4\t\t\t3\t3\t180\t180\t200\n";
        String[] segments = line.split("\\s+");
        System.out.println("segments length" + segments.length);
        for (String segment : segments) {
            System.out.println(segment);
        }
    }
}
