
# 作业：编程实践，使用Java API操作HBase

#### 作业描述:
实践主要是建表，插入数据，删除数据，查询等功能。建立一个如下所示的表：

![img.png](student_table.png)

• 表名：$your_name:student

• 空白处自行填写, 姓名学号一律填写真实姓名和学号

• 服务器版本为2.1.0（hbase版本和服务器上的版本可以不一致，但尽量保证一致）

代码:
主程序:hbase.MyDriver

hbase结果:
![img.png](hbase_result.png)

