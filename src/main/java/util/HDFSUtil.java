package util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

/**
 * @author even
 * @Description:
 * @date 2020-03-19
 */
public class HDFSUtil {

    /**
     * @MethodName: checkPath
     * @Param: [hdfsPath, configuration, doRename, isDelete]
     * @Return: boolean
     * @Description:
     *  check 目标路径是否存在;
     *  存在且doRename为true则重命名路径,重命名为 原文件名+_+修改时间戳 (因为有些系统有命名约束，所以没用.)
     *  如果存在且doRename为false且isDelete为true，则删除原文件 [防止误删，所以先根据重命名再决定是否删除]
     **/
    public static boolean checkPath(Path path, Configuration configuration, boolean doRename, boolean isDelete) throws IOException {
        FileSystem fileSystem = FileSystem.get(configuration);

        if(fileSystem.exists(path)){
            if(doRename){
                long time = System.currentTimeMillis();
                fileSystem.rename(path,new Path( path.toString()+"_"+time));
            }else {
                if(isDelete){
                    fileSystem.delete(path,true);
                }
            }
            return true;
        }else {
            return false;
        }
    }

    public static boolean checkPath(String Path, Configuration configuration, boolean doRename, boolean isDelete) throws IOException {
        return checkPath(new Path(Path),configuration,doRename,isDelete);
    }

    public static boolean checkPath(String Path, Configuration configuration, boolean doRename) throws IOException {
        return checkPath(new Path(Path),configuration,doRename,false);
    }

    public static boolean checkPath(Path path, Configuration configuration, boolean doRename) throws IOException {
        return checkPath(path,configuration,doRename,false);
    }





}
