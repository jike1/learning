
# Hive 作业

### 题目一

简单：展示电影ID为2116这部电影各年龄段的平均影评分

sql:
    
    SELECT u.age age,avg(r.rate) avgrate FROM t_user u 
    JOIN t_rating r ON u.userid = r.userid
    WHERE r.movieid=2116  GROUP BY u.age

Query ID = hive_20210803164024_6ae514d2-635f-4850-bb43-3440f35c9423

结果:
![img.png](topic_1.png)

### 题目二

中等：找出男性评分最高且评分次数超过50次的10部电影，展示电影名，平均影评分和评分次数

sql:
    
    SELECT u.sex sex,m.moviename name,avg(rate) avgrate,count(m.moviename) total 
    FROM t_movie m
    JOIN t_rating r ON r.movieid = m.movieid
    JOIN t_user u ON u.userid = r.userid
    WHERE u.sex='M'  
    GROUP BY u.sex,m.moviename
    HAVING total>50  //聚合结果的过滤需要使用HAVING 
    ORDER BY avgrate DESC
    LIMIT 10 

Query ID = hive_20210803170413_2a5395e5-5a39-4dbc-b6ab-034b0879746d

结果:
![img.png](topic_2.png)

### 题目三

困难：找出影评次数最多的女士所给出最高分的10部电影的平均影评分，展示电影名和平均影评分（可使用多行SQL）

sql:

    SELECT tm.moviename,avg(tr.rate)
    FROM t_movie tm
    JOIN t_rating tr ON tm.movieid=tr.movieid
    WHERE tm.movieid
    IN
    (	
        SELECT id FROM (
            SELECT m.movieid id,r.rate
            FROM t_movie m
            JOIN t_rating r ON m.movieid=r.movieid
            WHERE r.userid IN
            (
                SELECT userid FROM
                (
                    SELECT u.userid userid,count(1) cnt
                    FROM t_user u
                    JOIN t_rating t ON u.userid=t.userid
                    WHERE u.sex='F'
                    GROUP BY u.userid
                    ORDER BY cnt DESC
                    LIMIT 1
                ) tb1
            )
            ORDER BY r.rate DESC
            LIMIT 10
        ) tb2
    )
    GROUP BY tm.moviename

Query ID = hive_20210803190401_0870e0de-813c-45ba-9059-eee06528c609

结果:
![img.png](topic_3-a1.png)
![img.png](topic_3-a2.png)
