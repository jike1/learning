package spark.sparkcore.invert

import org.apache.spark.{SparkConf, SparkContext}
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
 * @Description: ${todo}
 * @author even
 * @date 2021/8/28
 */
object InvertedIndex {

  def main(args:Array[String]):Unit={

    val sc=new SparkContext(new SparkConf().setMaster("local[1]").setAppName("spark"))

    val logRDD=sc.textFile("/Users/even/Desktop/miaozhen/com.jike.learning/src/main/java/spark/sparkcore/invert/context.txt")

    var lineNumber=0
    val result=logRDD.flatMap(line=>{
      val values=line.split(" ")
      val resultMap=new mutable.HashMap[String,ArrayBuffer[(Int,Int)]]()
      values.map(word=>{
        val value=resultMap.getOrElse(word,ArrayBuffer((lineNumber,0)))(0)._2
        resultMap.put(word,ArrayBuffer((lineNumber,value+1)))
      })
      lineNumber=lineNumber+1
      resultMap
    }).reduceByKey((x,y)=>{
      x+=y(0)
    }).sortByKey()
    result.collect().foreach(println)
  }
}
