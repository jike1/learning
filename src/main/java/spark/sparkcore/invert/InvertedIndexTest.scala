package spark.sparkcore

import org.junit.Test

import scala.collection.mutable.HashMap

/**
 * @Description: ${todo}
 * @author even
 * @date 2021/8/18
 */
class InvertedIndexTest {

  @Test
  def testInvertedIndex(): Unit ={
    val line = "it is what it is"
    val words = line.split(" ")
    val map = HashMap[String,Int]()
    words.map(w => {
      val value = map.getOrElse(w,0)
      map.put(w,value+1)
    })
    map.foreach(println)
  }

}
