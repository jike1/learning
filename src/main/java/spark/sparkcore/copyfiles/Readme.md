# 作业二 Distcp的spark实现

#### 作业描述:

    使用Spark实现Hadoop 分布式数据传输工具 DistCp (distributed copy)，只要求实现最基础的copy功
    能，对于-update、-diff、-p不做要求
    对于HadoopDistCp的功能与实现，可以参考
    https://hadoop.apache.org/docs/current/hadoop-distcp/DistCp.html
    https://github.com/apache/hadoop/tree/release-2.7.1/hadoop-tools/hadoop-distcp
    Hadoop使用MapReduce框架来实现分布式copy，在Spark中应使用RDD来实现分布式copy
    应实现的功能为：
    sparkDistCp hdfs://xxx/source hdfs://xxx/target
    得到的结果为，启动多个task/executor，将hdfs://xxx/source目录复制到hdfs://xxx/target，得到
    hdfs://xxx/target/source
    需要支持source下存在多级子目录
    需支持-i Ignore failures 参数
    需支持-m max concurrence参数，控制同时copy的最大并发task数

代码:
主程序:spark.sparkcore.copyfiles.DistCpBySpark

