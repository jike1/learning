package spark.sparkcore.copyfiles

import org.apache.hadoop.fs.{FileStatus, FileSystem, FileUtil, Path}
import spark.util.SparkUtil

/**
 * @Description:
 * @author even
 * @date 2021/8/29
 */
object DistCpBySpark {

  val sc = SparkUtil.getSparkContext()
  val fileSystem = FileSystem.get(sc.hadoopConfiguration)

  def main(args: Array[String]): Unit = {

    val sourcePath = args(0)
    val targetPath = args(1)

    if(!fileSystem.exists(new Path(targetPath))){
      fileSystem.mkdirs(new Path(targetPath))
    }

    distcp(sourcePath,targetPath)
  }

  def distcp( sourcePath: String, targetPath: String) : Unit = {
    val files = fileSystem.listStatus(new Path(sourcePath))
    files.foreach(file => {
      if(file.isDirectory){
        fileSystem.mkdirs(new Path(targetPath + "/" + file.getPath.getName))
        distcp(sourcePath+"/"+file.getPath,targetPath + "/" + file.getPath.getName)
      }else{
        copyFile(file,targetPath)
      }
    })
  }


  //TODO
  def copyFile(file: FileStatus, target: String) = {

  }

}
