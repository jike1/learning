package spark.util

import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.{Duration, Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * @Description:
  * @author even
  * @date 2020-08-26
  */
object SparkUtil {

  def getStreamingContext(): StreamingContext ={
    new StreamingContext(getSparkConf(),Seconds(3))
  }

  def getStreamingContext(batchDuration: Duration): StreamingContext ={
    new StreamingContext(getSparkConf(),batchDuration)
  }

  def getStreamingContext(master : String ,appName : String,batchDuration: Duration): StreamingContext ={
    new StreamingContext(getSparkConf(master,appName),batchDuration)
  }

  def getSparkConf(): SparkConf ={
    val sparkConf = new SparkConf().setMaster("local[*]").setAppName("spark")
    sparkConf
  }

  def getSparkConf(master : String ,appName : String): SparkConf ={
    val sparkConf = new SparkConf().setMaster(master).setAppName(appName)
    sparkConf
  }

  def getSparkContext(master : String ,appName : String): SparkContext ={
    new SparkContext(getSparkConf(master,appName))
  }

  def getSparkContext(): SparkContext ={
    new SparkContext(getSparkConf)
  }

  def getSparkSession() : SparkSession = {
    SparkSession.builder().config(getSparkConf).getOrCreate()
  }

  def getSparkSession(master : String ,appName : String): SparkSession ={
    SparkSession.builder().config(getSparkConf(master,appName)).getOrCreate()
  }


}
